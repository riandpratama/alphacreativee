<!DOCTYPE html>

<html lang="en">
<style type="text/css">
	.whatsapp-button{
    position: fixed;
    bottom: 80px;
    right: 15px;
    z-index: 99;
    background-color: #25d366;
    border-radius: 50px;
    color: #ffffff;
    text-decoration: none;
    width: 30px;
    height: 30px;
    font-size: 30px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    -webkit-box-shadow: 0px 0px 25px -6px rgba(0,0,0,1);
    -moz-box-shadow: 0px 0px 25px -6px rgba(0,0,0,1);
    box-shadow: 0px 0px 25px -6px rgba(0,0,0,1);
    animation: effect 5s infinite ease-in;
}

@keyframes effect {
    20%, 100% {
        width: 50px;
        height: 50px;
        font-size: 30px;
    }
    0%, 10%{
        width: 55px;
        height: 55px;
        font-size: 35px;
    }
    5%{
        width: 50px;
        height: 50px;
        font-size: 30px;
    }
}
</style>

@include('frontend.template.partials._head')

<body>

	@include('frontend.template.partials._header')

	@yield('content')
	<a target="_blank" href="https://api.whatsapp.com/send?phone=6281233654547&text=" class="whatsapp-button"><i class="fab fa-whatsapp"></i></a>
	@include('frontend.template.partials._footer')

    @include('frontend.template.partials._javascript')
	
</body>

</html>
