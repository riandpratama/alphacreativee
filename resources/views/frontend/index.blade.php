@extends('frontend.template.default')

@section('content')
<section id="intro" class="clearfix">
    <div class="container">

      <div class="intro-img">
        <img src="{{ asset('img/main.png') }}" alt="" class="img-fluid">
      </div>

      <div class="intro-info">
        <h2>Kami bisa membantu dalam bisnis, Anda <i class="fa fa-smile"></i></h2>
        <div>
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="#services" class="btn-services scrollto">Our Services</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>About Us</h3>
        </header>

        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <p>
            Kemajuan teknologi seperti sekarang ini, AlphaCreativee menyediakan beberapa jasa untuk memenuhi kebutuhan Anda dalam pembuatan di bidang IT secara profesional yang sudah lengkap dengan berbagai fitur dan juga keamanan yang tinggi yang siap menjadi patner terbaik anda untuk menghadapi perkembangan jaman yang serba digital seperti sekarang ini..
            </p>

            <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-mobile"></i></div>
              <h4 class="title"><a href="">Website dan Aplikasi Mobile</a></h4>
              <p class="description">Website dan Aplikasi Mobile sebuah teknologi elektronik yang menghubungkan antara pengguna satu dan pengguna lainnya melalui media elektronik.</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-photo"></i></div>
              <h4 class="title"><a href="">Desaingrafis dan 3D</a></h4>
              <p class="description">Dengan perubahan dunia yang bergerak sangat cepat, hari ini variasi desain yang berkualitas sangat dibutuhkan. AlphaCreativee dapat membantu Anda untuk mengatasi hal tersebut. Kami menawarkan juga solusi terbaik dengan tim yang bekerja secara profesional.</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-camera"></i></div>
              <h4 class="title"><a href="">Fotografi dan Videografi</a></h4>
              <p class="description">Untuk membuat karya atau produk Anda dikenal dunia salah satu syarat yang harus dicapai adalah memamerkan gambar. Foto dan Video yang kami sediakan tentu akan sangat menarik banyak perhatian banyak orang.</p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="{{ asset('img/about-us.png') }}" class="img-fluid" alt="">
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp">
            <img src="{{ asset('img/customer.png') }}" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
            <h4>EASY CUSTOMIZE</h4>
            <p>
              Anda akan bisa editing website Anda sendiri jika suatu saat ingin mengganti suasana website, misalnya mengganti warna background, logo, favicon, slider, dll. Sangat mudah
            </p>
            <h4>NEWS / BLOG</h4>
            <p>
              Fitur ini berguna untuk pemilik perusahaan menuliskan berita tentang perusahaanya, misalnya berita kegiatan amal, csr, dll. Sehingga nilai perusahaan Anda dimata pengunjung akan baik..
            </p>
            <h4>LIVE CHAT</h4>
            <p>
              Fitur live chat yang membuat website Anda menjadi hidup. Siap menerima order kapan saja pengunjung datang. Bisa diakses secara mobile dari Android/iOS ataupun dari browser.
            </p>
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
            <img src="{{ asset('img/customer2.png') }}" class="img-fluid" alt="">
          </div>

          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
            <h4>GALERI FOTO</h4>
            <p>
             Galeri foto adalah salah satu fitur terpenting dalam website perusahaan. Fitur ini menyediakan galeri foto perusahaan yang akan menambah nilai trust untuk perusahaan Anda.
            </p>
            <h4>WEBMAIL</h4>
            <p>
              Anda bisa juga menggunakan webmail Anda jika membutuhkan. Webmail adalah email dengan nama domain website Anda. Ini bagus untuk citra usaha Anda.
            </p>
            <h4>KECEPATAN AKSES TERBAIK</h4>
            <p>
              Untuk paket terendah, hosting kami sudah dilengkapi dengan RAM 1024mb. Untuk paket diatasnya sudah dilengkapi RAM 2GB dan 4 GB. Theme yang digunakan juga sudah dilengkapi plugin cache dan image compressor otomatis, sehingga sangat ringan diakses.
            </p>
          </div>
          
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Services</h3>
        </header>

        <div class="row">
          @foreach($service as $item)
          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-analytics-outline" style="color: #ff689b;"></i></div>
              <h4 class="title"><a href=""> {{ $item->title }}</a></h4>
              <p class="description">{{ $item->description_main }}</p>
              <!-- Large modal -->
              <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg" style="float:right;" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="show(this);" data-item="{{$item}}">Detail</button>
            </div>
          </div>
          @endforeach
        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <header class="section-header">
          <h3>Why choose us?</h3>
          <p>Kami adalah jasa konsultan IT yang memiliki passion di bidang IT dan bekerja secara professional dalam meangani kasus-kasus pelanggan.</p>
        </header>

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fas fa-coins"></i>
              <div class="card-body">
                <h5 class="card-title">Harga Fleksibel</h5>
                <p class="card-text">Penentuan harga sesuai dengan kebutuhan pelanggan. Terdapat nota kesepahaman dan perjanjian kontrak, sehingga jasa yang Kami berikan aman dan berlandaskan hukum. </p>
                
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fas fa-user-friends"></i>
              <div class="card-body">
                <h5 class="card-title">Free Konsultasi</h5>
                <p class="card-text">Melakukan identifikasi yang akurat kemudian memberikan solusi terhadap kebutuhan pelanggan didukung dengan tim yang berpengalaman.</p>
                
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-object-group"></i>
              <div class="card-body">
                <h5 class="card-title">Desain</h5>
                <p class="card-text">Menyediakan desain menarik, up to date sesuai dengan aplikasi kebutuhan pelanggan. </p>
                
              </div>
            </div>
          </div>

        </div>

        <div class="row counters">

          <div class="col-lg-4 col-6 text-center">
            <span data-toggle="counter-up">57</span>
            <p>Clients</p>
          </div>

          <div class="col-lg-4 col-6 text-center">
            <span data-toggle="counter-up">182</span>
            <p>Projects</p>
          </div>

          <div class="col-lg-4 col-6 text-center">
            <span data-toggle="counter-up">18</span>
            <p>Hard Workers</p>
          </div>
  
        </div>

      </div>
    </section>

    <!--==========================
      Portfolio Section
    ============================-->
    <section id="portfolio" class="clearfix">
      <div class="container">

        <header class="section-header">
          <h3 class="section-title">Our Portfolio</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">DESIGN</li>
              <li data-filter=".filter-web">Web</li>
              <li data-filter=".filter-foto">Foto</li>
              <li data-filter=".filter-video">Video</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">
        @foreach ($portfolio as $item)
          <div class="col-lg-4 col-md-6 portfolio-item {{ $item->filter }}">
            <div class="portfolio-wrap">
              <img src="{{ asset('storage/'.$item->image) }}" class="img-fluid" alt="{{ $item->name }}">
              <div class="portfolio-info">
                <h4><a href="">{{ $item->name }}</a></h4>
                <div>
                  <a href="{{ asset('storage/'.$item->image) }}" data-lightbox="portfolio" data-title="{{ $item->name }}" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
        @endforeach
        </div>

      </div>
    </section><!-- #portfolio -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="testimonials" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Testimonials</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-12">

            <div class="owl-carousel testimonials-carousel wow fadeInUp">
            @foreach ($testimonial as $item)
              <div class="testimonial-item">
                <h3>{{ $item->name }}</h3>
                <h4>{{ $item->status }}</h4>
                <p><b>"
                  {{ $item->message }}
                  "</b>
                </p>
              </div>
            @endforeach
            </div>

          </div>
        </div>


      </div>
    </section><!-- #testimonials -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="section-bg">

      <div class="container">

        <div class="section-header">
          <h3>Technologies we use</h3>
          <p>Alphacreativee berusaha untuk selalu menggunakan teknologi terbaru dalam membangun sistem, Alphacreativee memberikan rasa nyaman kepada semua pelanggan kami.</p>
        </div>

        <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

          <div class="col-lg-2 col-md-3 col-xs-4">
            <div class="client-logo">
              <img src="{{ asset('image-teknologi/nginx.png') }}" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-2 col-md-3 col-xs-4">
            <div class="client-logo">
              <img src="{{ asset('image-teknologi/laravel.png') }}" class="img-fluid" alt="">
            </div>
          </div>
        
          <div class="col-lg-2 col-md-3 col-xs-4">
            <div class="client-logo">
              <img src="{{ asset('image-teknologi/vuejs.png') }}" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-2 col-md-3 col-xs-4">
            <div class="client-logo">
              <img src="{{ asset('image-teknologi/bootstrap.png') }}" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-2 col-md-3 col-xs-4">
            <div class="client-logo">
              <img src="{{ asset('image-teknologi/android.png') }}" class="img-fluid" alt="">
            </div>
          </div>
        
          <div class="col-lg-2 col-md-3 col-xs-4">
            <div class="client-logo">
              <img src="{{ asset('image-teknologi/postgresql.png') }}" class="img-fluid" alt="">
            </div>
          </div>

        </div>

      </div>

    </section>

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container">

        <div class="section-header">
          <h3>Contact Us</h3>
        </div>

        <div class="row wow fadeInUp">

          <div class="col-lg-6">
            <div class="map mb-4 mb-lg-0">

              <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Jl.%20Jambangan%20I%206-11%2C%20Jambangan%2C%20Kec.%20Jambangan%2C%20Kota%20SBY%2C%20Jawa%20Timur%2060232&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>Google Maps Generator by <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:400px}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:500px;}</style></div>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="row">
              
              <div class="col-md-6 info">
                <i class="ion-ios-email-outline"></i>
                <p>alphacreativee007@gmail.com</p>
                <i class="ion-ios-telephone-outline"></i>
                <p>0812 3365 4547</p>
              </div>
              {{-- <div class="col-md-3 info">
                <i class="ion-ios-telephone-outline"></i>
                <p>0812 3365 4547</p>
              </div> --}}
            </div>

            <div class="form">
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form action="{{ route('contactus') }}" method="post" role="form" class="contactForm">
                @csrf
                
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"><p id="title"></p></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p id="description"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('js')

<script type="text/javascript">
  function show(button){
        var item = $(button).data('item');
        document.getElementById("description").innerHTML = item.description;
        document.getElementById("title").innerHTML = item.title;
    }
</script>

@endsection