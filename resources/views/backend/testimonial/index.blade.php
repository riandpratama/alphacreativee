@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Testimonial</div>
                
                <div class="panel-body">
                    <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i>&nbsp;Tambah Testimonial </a>
					
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Image </th>
                                    <th>Nama Image</th>
                                    <th>Filter </th>
                                    <th width="100px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}</td>
                                    
                                    <td>{{ $item->name }}</td>
                                    <td><span class="badged-primary"></span>{{ $item->status }}</td>
                                    <td>{{ $item->message }}</td>
									<td>
                                    
                                    <p data-placement="top" data-toggle="tooltip" title="Edit" style="display:inline-block;" ><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="insertFormEdit(this);" data-item="{{$item}}"><span class="glyphicon glyphicon-pencil"></span></button></p>
    								
    								<p data-placement="top" data-toggle="tooltip" title="Delete" style="display:inline-block;"> <form action="{{ route('testimonial.destroy', $item->id) }}" method="GET" style="display:inline-block;">
                                             <button title="Delete" class="btn btn-danger js-submit-confirm btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span> </button>
                                        </form></p>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    	<div class="modal-content">
          <div class="modal-header">
          	<h4><i class="fa fa-plus"></i> Tambah data</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	      </div>
	          <div class="modal-body">
	          	<form action="{{ route('testimonial.store') }}" method="POST" enctype="multipart/form-data">
              		{{ csrf_field() }}
	                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	                    <label for="name">Nama</label>
	                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
	                    @if ($errors->has('name'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('name') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
	                    <label for="status">Status</label>
	                    <input id="status" type="text" class="form-control" name="status" value="{{ old('status') }}">
	                    @if ($errors->has('status'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('status') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
	                    <label for="message">Message</label>
	                    <input id="message" type="text" class="form-control" name="message" value="{{ old('message') }}">
	                    @if ($errors->has('message'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('message') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="modal-footer">
	        			<button type="submit" class="btn btn-primary btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-save"></span> Simpan</button>
	        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
	      	  		</div> 
	      	  	</form>
	      	  </div>
        </div>
    <!-- /.modal-content --> 
  	</div>
      <!-- /.modal-dialog modal-dialog-centered --> 
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    	<div class="modal-content">
          <div class="modal-header">
          	<h4><i class="fa fa-edit"></i> Ubah data</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	      </div>
	          <div class="modal-body">
	          	<form role="form" method="POST" id="formEdit" enctype="multipart/form-data">
	                {{ csrf_field() }}
	                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	                    <label for="name">Nama</label>
	                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
	                    @if ($errors->has('name'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('name') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
	                    <label for="status">Filter</label>
	                    <input id="status" type="text" class="form-control" name="status" value="{{ old('status') }}">
	                </div>
	                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
	                    <label for="message">Message</label>
	                    <input id="message" type="text" class="form-control" name="message" value="{{ old('message') }}">
	                    @if ($errors->has('message'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('message') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="modal-footer">
	        			<button type="submit" class="btn btn-warning btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
	        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
	      	  		</div>  
	      	  </div>
	      	</form>
        </div>
    <!-- /.modal-content --> 
  	</div>
      <!-- /.modal-dialog modal-dialog-centered --> 
</div>

@endsection

@section('js')
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#data').dataTable();
	} );

	function insertFormEdit(button){
        var item = $(button).data('item');
        console.log(item);
        console.log($('#formEdit .form-group #facebook_edit'));
        $('form#formEdit').attr('action','{{ url("admin/testimonial") }}/'+item.id+'/update');
        $('#formEdit .form-group #name').val(item.name);
        $('#formEdit .form-group #status').val(item.status);
        $('#formEdit .form-group #message').val(item.message);
    }
  </script>
@endsection