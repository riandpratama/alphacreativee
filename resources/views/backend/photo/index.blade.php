@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Photo</div>
                
                <div class="panel-body">
                    <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i>&nbsp;Tambah Photo </a>
					
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Photo </th>
                                    <th>Nama </th>
                                    <th>Overflay </th>
                                    <th>Facebook </th>
                                    <th>Gmail </th>
                                    <th>Linkedin </th>
                                    <th width="100px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}</td>
                                    <td><img src="{{ asset('storage/'. $item->photo) }}" alt="{{ $item->name }}" width="100px" height="100px"></td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->overlay }}</td>
                                    <td>{{ $item->facebook }}</td>
                                    <td>{{ $item->gmail }}</td>
                                    <td>{{ $item->linkedin }}</td>
									<td>
                                    
                                    <p data-placement="top" data-toggle="tooltip" title="Edit" style="display:inline-block;" ><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="insertFormEdit(this);" data-item="{{$item}}"><span class="glyphicon glyphicon-pencil"></span></button></p>
    								
    								<p data-placement="top" data-toggle="tooltip" title="Delete" style="display:inline-block;"> <form action="{{ route('photo.destroy', $item->id) }}" method="GET" style="display:inline-block;">
                                             <button title="Delete" class="btn btn-danger js-submit-confirm btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span> </button>
                                        </form></p>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    	<div class="modal-content">
          <div class="modal-header">
          	<h4><i class="fa fa-plus"></i> Tambah data</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	      </div>
	          <div class="modal-body">
	          	<form action="{{ route('photo.store') }}" method="POST" enctype="multipart/form-data">
              		{{ csrf_field() }}
              		<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
	                    <label for="photo">Photo</label>
	                    <input id="photo" type="file" class="form-control" name="photo" value="{{ old('photo') }}">
	                    @if ($errors->has('photo'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('photo') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	                    <label for="name">Nama</label>
	                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
	                    @if ($errors->has('name'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('name') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('overlay') ? ' has-error' : '' }}">
	                    <label for="overlay">Overlay</label>
	                    <select name="overlay" id="" class="form-control">
	                    	<option value="0.1s">0.1s</option>
	                    	<option value="0.2s">0.2s</option>
	                    	<option value="0.3s">0.3s</option>
	                    </select>
	                </div>
	                <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
	                    <label for="facebook">Akun Facebook</label>
	                    <input id="facebook" type="text" class="form-control" name="facebook" value="{{ old('facebook') }}">
	                    @if ($errors->has('facebook'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('facebook') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('gmail') ? ' has-error' : '' }}">
	                    <label for="gmail">Akun Gmail</label>
	                    <input id="gmail" type="text" class="form-control" name="gmail" value="{{ old('gmail') }}">
	                    @if ($errors->has('gmail'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('gmail') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('linkedin') ? ' has-error' : '' }}">
	                    <label for="linkedin">Akun Linkedin</label>
	                    <input id="linkedin" type="text" class="form-control" name="linkedin" value="{{ old('linkedin') }}">
	                    @if ($errors->has('linkedin'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('linkedin') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="modal-footer">
	        			<button type="submit" class="btn btn-primary btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-save"></span> Simpan</button>
	        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
	      	  		</div> 
	      	  	</form>
	      	  </div>
        </div>
    <!-- /.modal-content --> 
  	</div>
      <!-- /.modal-dialog modal-dialog-centered --> 
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    	<div class="modal-content">
          <div class="modal-header">
          	<h4><i class="fa fa-edit"></i> Ubah data</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	      </div>
	          <div class="modal-body">
	          	<form role="form" method="POST" id="formEdit" enctype="multipart/form-data">
	                {{ csrf_field() }}
	                <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
	                    <label for="photo">Photo</label>
	                    <input id="photo" type="file" class="form-control" name="photo" value="{{ old('photo') }}">
	                    @if ($errors->has('photo'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('photo') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	                    <label for="name">Nama</label>
	                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
	                    @if ($errors->has('name'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('name') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('overlay') ? ' has-error' : '' }}">
	                    <label for="overlay">Overlay</label>
	                    <select name="overlay" id="" class="form-control">
	                    	<option value="0.1s">0.1s</option>
	                    	<option value="0.2s">0.2s</option>
	                    	<option value="0.3s">0.3s</option>
	                    </select>
	                </div>
	                <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
	                    <label for="facebook">Akun Facebook</label>
	                    <input id="facebook_edit" type="text" class="form-control" name="facebook" value="{{ old('facebook') }}">
	                    @if ($errors->has('facebook'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('facebook') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('gmail') ? ' has-error' : '' }}">
	                    <label for="gmail">Akun Gmail</label>
	                    <input id="gmail" type="text" class="form-control" name="gmail" value="{{ old('gmail') }}">
	                    @if ($errors->has('gmail'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('gmail') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('linkedin') ? ' has-error' : '' }}">
	                    <label for="linkedin">Akun Linkedin</label>
	                    <input id="linkedin" type="text" class="form-control" name="linkedin" value="{{ old('linkedin') }}">
	                    @if ($errors->has('linkedin'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('linkedin') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="modal-footer">
	        			<button type="submit" class="btn btn-warning btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
	        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
	      	  		</div>  
	      	  </div>
	      	</form>
        </div>
    <!-- /.modal-content --> 
  	</div>
      <!-- /.modal-dialog modal-dialog-centered --> 
</div>

@endsection

@section('js')
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#data').dataTable();
	} );

	function insertFormEdit(button){
        var item = $(button).data('item');
        console.log(item);
        console.log($('#formEdit .form-group #facebook_edit'));
        $('form#formEdit').attr('action','{{ url("admin/photo") }}/'+item.id+'/update');
        $('#formEdit .form-group #name').val(item.name);
        $('#formEdit .form-group #overlay').val(item.overlay);
        $('#formEdit .form-group #facebook_edit').val(item.facebook);
        $('#formEdit .form-group #gmail').val(item.gmail);
        $('#formEdit .form-group #linkedin').val(item.linkedin);
    }
  </script>
@endsection