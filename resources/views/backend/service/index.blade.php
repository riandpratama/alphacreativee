@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Service</div>
                
                <div class="panel-body">
                    <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i>&nbsp;Tambah Service </a>
					
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title </th>
                                    <th>Description Main</th>
                                    <th>Description Detail</th>
                                    <th>Icon </th>
                                    <th width="100px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->description_main }}</td>
                                    <td>{!! $item->description !!}</td>
                                    <td>{{ $item->icon }}</td>
									<td>
                                    
                                    <p data-placement="top" data-toggle="tooltip" title="Edit" style="display:inline-block;" ><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="insertFormEdit(this);" data-item="{{$item}}"><span class="glyphicon glyphicon-pencil"></span></button></p>
    								
    								<p data-placement="top" data-toggle="tooltip" title="Delete" style="display:inline-block;"> <form action="{{ route('service.destroy', $item->id) }}" method="GET" style="display:inline-block;">
                                             <button title="Delete" class="btn btn-danger js-submit-confirm btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span> </button>
                                        </form></p>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    	<div class="modal-content">
          <div class="modal-header">
          	<h4><i class="fa fa-plus"></i> Tambah data</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	      </div>
	          <div class="modal-body">
	          	<form action="{{ route('service.store') }}" method="POST" enctype="multipart/form-data">
              		{{ csrf_field() }}
              		<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
	                    <label for="title">Tittle</label>
	                    <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">
	                    @if ($errors->has('title'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('title') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('description_main') ? ' has-error' : '' }}">
	                    <label for="description_main">Deskripsi Utama</label>
	                    <input id="description_main" type="text" class="form-control" name="description_main" value="{{ old('description_main') }}">
	                    @if ($errors->has('description_main'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('description_main') }}</strong>
	                        </span>
	                    @endif
	                </div>
              		<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
	                    <label for="description">Deskripsi Detail</label>
	                    <textarea name="description" id="" cols="50" rows="20"></textarea>
	                    @if ($errors->has('description'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('description') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
	                    <label for="icon">Icon</label>
	                    <input id="icon" type="text" class="form-control" name="icon" value="{{ old('icon') }}" placeholder="ion-ios-paper-outline">
	                </div>
	                <div class="modal-footer">
	        			<button type="submit" class="btn btn-primary btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-save"></span> Simpan</button>
	        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
	      	  		</div> 
	      	  	</form>
	      	  </div>
        </div>
    <!-- /.modal-content --> 
  	</div>
      <!-- /.modal-dialog modal-dialog-centered --> 
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    	<div class="modal-content">
          <div class="modal-header">
          	<h4><i class="fa fa-edit"></i> Ubah data</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	      </div>
	          <div class="modal-body">
	          	<form role="form" method="POST" id="formEdit" enctype="multipart/form-data">
	                {{ csrf_field() }}
	                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
	                    <label for="title">Tittle</label>
	                    <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">
	                    @if ($errors->has('title'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('title') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('description_main') ? ' has-error' : '' }}">
	                    <label for="description_main">Deskripsi Utama</label>
	                    <input id="description_main" type="text" class="form-control" name="description_main" value="{{ old('description_main') }}">
	                    @if ($errors->has('description_main'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('description_main') }}</strong>
	                        </span>
	                    @endif
	                </div>
              		<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
	                    <label for="description">Deskripsi Detail</label>
	                    <textarea name="description" id="description" cols="50" rows="20"></textarea>
	                    @if ($errors->has('description'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('description') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
	                    <label for="icon">Icon</label>
	                    <input id="icon" type="text" class="form-control" name="icon" value="{{ old('icon') }}" placeholder="ion-ios-paper-outline">
	                </div>
	                <div class="modal-footer">
	        			<button type="submit" class="btn btn-warning btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
	        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
	      	  		</div>  
	      	  </div>
	      	</form>
        </div>
    <!-- /.modal-content --> 
  	</div>
      <!-- /.modal-dialog modal-dialog-centered --> 
</div>

@endsection

@section('js')
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#data').dataTable();
	} );

	function insertFormEdit(button){
        var item = $(button).data('item');
        console.log(item);
        console.log($('#formEdit .form-group #facebook_edit'));
        $('form#formEdit').attr('action','{{ url("admin/service") }}/'+item.id+'/update');
        $('#formEdit .form-group #title').val(item.title);
        $('#formEdit .form-group #description_main').val(item.description_main);
        $('#formEdit .form-group #description').val(item.description);
        $('#formEdit .form-group #icon').val(item.icon);
    }
  </script>
  <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
  <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
  <script>
    $('textarea').ckeditor({
        "filebrowserImageBrowseUrl" : '/laravel-filemanager?type=Images',
        "filebrowserBrowseUrl" : "/laravel-filemanager?type=Files"
    });
  </script>
@endsection