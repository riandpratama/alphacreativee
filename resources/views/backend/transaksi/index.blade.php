
@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Transaksi</div>
                
                <div class="panel-body">
                    <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i>&nbsp;Tambah Transaksi </a>
					
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal </th>
                                    <th>User</th>
                                    <th>Transaksi</th>
                                    <th>Pemasukkan </th>
                                    <th>Pengeluaran </th>
                                    <th>Saldo </th>
                                </tr>
                            </thead>
                            <tbody>
                            	@php
                            		$debet = 0;
                            		$kredit = 0;
                            		$saldo = 0;
                            	@endphp
                                @foreach($data as $item)
                                @php 
	                                $debet += $item->debet; 
	                                $kredit += $item->kredit;
	                                $saldo = $item->saldo;
	                            @endphp
                                <tr>
                                	<td>{{ $loop->iteration }}.</td>
                                    <td>{{ date('d/m/Y',strtotime($item->tanggal)) }}</td>
                                    <td>{{ $item->user }}</td>
                                    <td>{{ $item->transaksi }}</td>
                                    <td>Rp. {{ number_format($item->debet) }}</td>
                                    <td>Rp. {{ number_format($item->kredit) }}</td>
                                    <td>Rp. {{ number_format($item->saldo) }}</td>
                                </tr>
                                @endforeach

                                @if ($data->where('id', $data->max('id'))->first())
									<p data-placement="top" data-toggle="tooltip" title="Delete" style="display:inline-block;"> 
										<form action="{{ route('transaksi.destroy', $item->id) }}" method="GET" >
	                                        <button title="Delete" class="btn btn-danger btn-sm" type="submit" onclick="return confirm('Anda yakin ingin menghapus data?')">
	                                        	<span class="glyphicon glyphicon-trash"></span> Hapus Transaksi Terakhir
	                                        </button>
	                                    </form>
	                                </p>
	                            @endif
                            </tbody>
                            <tfoot>
                            	<tr>
                            		<td colspan="4" style="text-align: center;" ><b>Total</b></td>
                            		<td><b>Rp. {{ number_format($debet) }}</b></td>
                            		<td><b>Rp. {{ number_format($kredit) }}</b></td>
                            		<td><b>Rp. {{ number_format($saldo) }}</b></td>
                            	</tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Total Pemegang</div>
                
                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Nama User</th>
                                <th>Total</th>
                            </tr>
                            <tr>
                                <th>1</th>
                                <th>Prada</th>
                                <th>Rp. {{ number_format($totalPrada) }}</th>
                            </tr>
                            <tr>
                                <th>2</th>
                                <th>Muhlas</th>
                                <th>Rp. {{ number_format($totalMuhlas) }}</th>
                            </tr>
                            <tr>
                                <th>3</th>
                                <th>Riand</th>
                                <th>Rp. {{ number_format($totalRiand) }}</th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    	<div class="modal-content">
          <div class="modal-header">
          	<h4><i class="fa fa-plus"></i> Tambah data</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	      </div>
	          <div class="modal-body">
	          	<form action="{{ route('transaksi.store') }}" method="POST" enctype="multipart/form-data">
              		{{ csrf_field() }}
	                <div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
	                    <label for="tanggal">Tanggal</label>
	                    <input id="tanggal" type="date" class="form-control" name="tanggal" required="" value="{{ old('tanggal') }}">
	                    @if ($errors->has('tanggal'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('tanggal') }}</strong>
	                        </span>
	                    @endif
	                </div>
                    <div class="form-group{{ $errors->has('user') ? ' has-error' : '' }}">
                        <label for="user">Pengeluaran atau Pemasukkan dari</label>
                        <select name="user" class="form-control" id="">
                            <option value="Prada">Prada</option>
                            <option value="Muhlas">Muhlas</option>
                            <option value="Riand">Riand</option>
                        </select>
                        @if ($errors->has('user'))
                            <span class="help-block">
                                <strong>{{ $errors->first('user') }}</strong>
                            </span>
                        @endif
                    </div>
	                <div class="form-group{{ $errors->has('transaksi') ? ' has-error' : '' }}">
	                    <label for="transaksi">Keterangan Transaksi</label>
	                    <input id="transaksi" type="text" class="form-control" name="transaksi" required="" value="{{ old('transaksi') }}">
	                    @if ($errors->has('transaksi'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('transaksi') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('debet') ? ' has-error' : '' }}">
	                    <label for="debet">Pemasukkan</label>
	                    <input id="debet" type="number" class="form-control" name="debet" value="{{ old('debet') }}">
	                    <p>*Isi Ketika mendapatkan Pemasukkan</p>
	                    @if ($errors->has('debet'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('debet') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('kredit') ? ' has-error' : '' }}">
	                    <label for="kredit">Pengeluaran</label>
	                    <input id="kredit" type="number" class="form-control" name="kredit" value="{{ old('kredit') }}">
	                    <p>*Isi Ketika mendapatkan Pengeluaran</p>
	                    @if ($errors->has('kredit'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('kredit') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="modal-footer">
	        			<button type="submit" class="btn btn-primary btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-save"></span> Simpan</button>
	        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
	      	  		</div> 
	      	  	</form>
	      	  </div>
        </div>
    <!-- /.modal-content --> 
  	</div>
      <!-- /.modal-dialog modal-dialog-centered --> 
</div>

@endsection

@section('js')
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#data').dataTable();
	} );

	function insertFormEdit(button){
        var item = $(button).data('item');
        console.log(item);
        console.log($('#formEdit .form-group #facebook_edit'));
        $('form#formEdit').attr('action','{{ url("admin/testimonial") }}/'+item.id+'/update');
        $('#formEdit .form-group #name').val(item.name);
        $('#formEdit .form-group #status').val(item.status);
        $('#formEdit .form-group #message').val(item.message);
    }
  </script>
@endsection