<?php

namespace App\Http\Controllers;

use Alert;
use Storage;
use App\Models\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Portfolio::orderBy('created_at', 'desc')->get();

        return view('backend.portfolio.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'filter' => 'required',
            'name' => 'required',
            'image' => 'required',
        ]);

        if ($request->hasFile('image')) {
            $filename = $request->file('image')->store('uploads/portfolio');
        } else {
            $filename = NULL;
        }

        $portfolio = Portfolio::create([
            'filter' => $request->filter,
            'name' => $request->name,
            'image' => $filename,
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $portfolio = Portfolio::findOrFail($id);

        if ($request->file('image') !== null && $request->file('image') !== ''){
            Storage::delete($portfolio->image);
        }
        if ($request->hasFile('image')) {
            $filename = $request->file('image')->store('uploads/portfolio');
        } else {
            $filename = $portfolio->image;
        }

        $portfolio->name = $request->name;
        $portfolio->filter = $request->filter;
        $portfolio->image = $filename;
        $portfolio->save();

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::findOrFail($id);

        Storage::delete($portfolio->photo);
        $portfolio->delete();

        Alert::success('Success!');

        return redirect()->back();
    }
}
