<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Photo;
use App\Models\Service;
use App\Models\Portfolio;
use App\Models\ContactUs;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service = Service::orderBy('title', 'ASC')->get();
    	$photo = Photo::orderBy('created_at', 'ASC')->get();
        $portfolio = Portfolio::orderBy('created_at', 'ASC')->get();
        $testimonial = Testimonial::orderBy('created_at', 'ASC')->get();

        return view('frontend.index', compact('photo', 'portfolio', 'testimonial', 'service'));
    }

    public function contactus(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        ContactUs::create($request->all());

        return redirect()->back();
    }
}
