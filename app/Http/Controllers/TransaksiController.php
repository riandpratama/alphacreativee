<?php

namespace App\Http\Controllers;

use DB;
use Alert;
use App\Models\Transaksi;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Transaksi::orderBy('tanggal', 'asc')->get();

        $riand1 = Transaksi::select('debet')->where('user', 'Riand')->sum('debet');
        $riand2 = Transaksi::select('kredit')->where('user', 'Riand')->sum('kredit');
        $totalRiand = $riand1-$riand2;

        $muhlas1 = Transaksi::select('debet')->where('user', 'Muhlas')->sum('debet');
        $muhlas2 = Transaksi::select('kredit')->where('user', 'Muhlas')->sum('kredit');
        $totalMuhlas = $muhlas1-$muhlas2;

        $prada1 = Transaksi::select('debet')->where('user', 'Prada')->sum('debet');
        $prada2 = Transaksi::select('kredit')->where('user', 'Prada')->sum('kredit');
        $totalPrada = $prada1-$prada2;

        return view('backend.transaksi.index', compact('data', 'totalRiand', 'totalMuhlas', 'totalPrada'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'tanggal' => 'required',
            'transaksi' => 'required',
            'user' => 'required',
        ]);
        
        if ($request->debet != NULL) {
            if (DB::table('transaksi')->count() > 0){
                $transaksi = DB::table('transaksi')->orderBy('created_at', 'desc')->first()->saldo;
            } else {
                $transaksi = 0;
            }
            
            Transaksi::create([
                'tanggal' => $request->tanggal,
                'user' => $request->user,
                'transaksi' => $request->transaksi,
                'debet' => $request->debet,
                'saldo' => $transaksi + $request->debet,
            ]);

        } else {
            $transaksi = DB::table('transaksi')->orderBy('created_at', 'desc')->first()->saldo;
            Transaksi::create([
                'tanggal' => $request->tanggal,
                'user' => $request->user,
                'transaksi' => $request->transaksi,
                'kredit' => $request->kredit,
                'saldo' => $transaksi - $request->kredit ,
            ]);
        }

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = Transaksi::findOrFail($id);

        $transaksi->delete();

        Alert::success('Success!');

        return redirect()->back();
    }
}
