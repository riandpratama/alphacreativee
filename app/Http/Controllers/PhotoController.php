<?php

namespace App\Http\Controllers;

use Alert;
use Storage;
use App\Models\Photo;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Photo::orderBy('created_at', 'desc')->get();

        return view('backend.photo.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'photo' => 'required',
        ]);

        if ($request->hasFile('photo')) {
            $filename = $request->file('photo')->store('uploads/photo');
        } else {
            $filename = NULL;
        }

        $photo = Photo::create([
            'name' => $request->name,
            'overlay' => $request->overlay,
            'facebook' => $request->facebook,
            'gmail' => $request->gmail,
            'linkedin' => $request->linkedin,
            'photo' => $filename,
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $photo = Photo::findOrFail($id);

        if ($request->file('photo') !== null && $request->file('photo') !== ''){
            Storage::delete($photo->photo);
        }
        if ($request->hasFile('photo')) {
            $filename = $request->file('photo')->store('uploads/photo');
        } else {
            $filename = $photo->photo;
        }

        $photo->name = $request->name;
        $photo->overlay = $request->overlay;
        $photo->facebook = $request->facebook;
        $photo->gmail = $request->gmail;
        $photo->linkedin = $request->linkedin;
        $photo->photo = $filename;
        $photo->save();

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);

        Storage::delete($photo->photo);
        $photo->delete();

        Alert::success('Success!');

        return redirect()->back();
    }
}
