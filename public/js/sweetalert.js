
$(document.body).on('click', '.js-submit-confirm', function(event) {
event.preventDefault();
var $form = $(this).closest('form');
swal({
    title: "Apakah anda yakin?",
    text: "Data yang telah di hapus tidak bisa di kembalikan",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: true
  },
  function() {
    $form.submit();
  });
});
