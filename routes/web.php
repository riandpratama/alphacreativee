<?php

Route::get('/cache', function(){
	Artisan::call('config:cache');
});

Route::group(['namespace' => 'Frontend'], function () {

	Route::get('/', 'MainController@index');

	Route::post('/contactus', 'MainController@contactus')->name('contactus');

});

// Auth::routes();

Route::get('/alpha/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'] ,function(){

	Route::get('photo', 'PhotoController@index');
	Route::post('photo/store', 'PhotoController@store')->name('photo.store');
	Route::post('photo/{id}/update', 'PhotoController@update')->name('photo.update');
	Route::get('photo/{id}/delete', 'PhotoController@destroy')->name('photo.destroy');

	Route::get('portfolio', 'PortfolioController@index');
	Route::post('portfolio/store', 'PortfolioController@store')->name('portfolio.store');
	Route::post('portfolio/{id}/update', 'PortfolioController@update')->name('portfolio.update');
	Route::get('portfolio/{id}/delete', 'PortfolioController@destroy')->name('portfolio.destroy');

	Route::get('testimonial', 'TestimonialsController@index');
	Route::post('testimonial/store', 'TestimonialsController@store')->name('testimonial.store');
	Route::post('testimonial/{id}/update', 'TestimonialsController@update')->name('testimonial.update');
	Route::get('testimonial/{id}/delete', 'TestimonialsController@destroy')->name('testimonial.destroy');

	Route::get('contactus', 'ContactUsController@index');

	Route::get('transaksi', 'TransaksiController@index');
	Route::post('transaksi/store', 'TransaksiController@store')->name('transaksi.store');
	Route::post('transaksi/{id}/update', 'TransaksiController@update')->name('transaksi.update');
	Route::get('transaksi/{id}/delete', 'TransaksiController@destroy')->name('transaksi.destroy');

	Route::get('service', 'ServiceController@index');
	Route::post('service/store', 'ServiceController@store')->name('service.store');
	Route::post('service/{id}/update', 'ServiceController@update')->name('service.update');
	Route::get('service/{id}/delete', 'ServiceController@destroy')->name('service.destroy');



});
